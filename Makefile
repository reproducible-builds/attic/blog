all: local

# build and serve locally, for testing
# view it at http://localhost:8000/blog/
serve: local
	@echo The local instance is available at http://127.0.0.1:8000/blog
	cd local && python -m SimpleHTTPServer

local: blog-local.setup
	ikiwiki --setup blog-local.setup

# build a local-only ikiwiki setup file. the file format is not very complex,
# so we use a minor hack - we embed local values in our main blog.setup file,
# in comments starting "##local:"
blog-local.setup: blog.setup Makefile
	sed -re 's/^(\S*): (.*)##local:\s*(.*)$$/\1: \3/g' "$<" > "$@"

clean:
	rm -rf local tags blog-local.setup .ikiwiki

.PHONY: all local serve clean
