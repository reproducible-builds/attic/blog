[[!if test="enabled(calendar)" then="""
[[!calendar pages="page(./posts/*) and !*/Discussion"]]
"""]]


[[Tags]]:
[[!pagestats style="list" pages="./tags/*" among="./posts/*"]]

Links:

 * Blog [[archives]]
 * [reproducible-builds.org](https://reproducible-builds.org)
   * [projects](https://reproducible-builds.org/who)
   * [tests.r-b.o](https://tests.reproducible-builds.org)
 * [Reproducible builds Debian wiki](https://wiki.debian.org/ReproducibleBuilds)
 * [SOURCE_DATE_EPOCH specification](https://reproducible-builds.org/specs/source-date-epoch/)

