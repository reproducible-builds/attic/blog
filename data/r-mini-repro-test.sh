#!/bin/sh

set -e

if [ -n "$TRACE" ]; then
	STRACECMD="strace -s 1024 -e file,write,execve -ff -o out.log"
fi

# the below code is adapted from various shell scripts in r-base
run() {
	set -x
	R_DEFAULT_PACKAGES= LC_COLLATE=C $STRACECMD \
	"/usr/lib/R/bin/R" $myArgs --slave --args ${args}
	{ set +x; } 2>/dev/null
}

install() {
	vanilla_install=false
	args=
	while test -n "${1}"; do
	  case ${1} in
		--no-vanilla)
		  vanilla_install=false ;;
		--use-vanilla)
		  vanilla_install=true ;;
	  *)
	  ## quote each argument here, unquote in R code.
	  args="${args}nextArg${1}"
	  ;;
	  esac
	  shift
	done

	myArgs='--no-restore'

	if [ -n "$DEBUG" ]; then
		echo 'enter (some or all of) the following into R:
debug(tools:::.install_package_Rd_objects)
debug(tools:::.parse_code_file)
debug(tools:::makeLazyLoadDB)
tools:::.install_packages()'
		run
	else
		echo 'tools:::.install_packages()' | run
	fi
}
# the above code is adapted from various shell scripts in r-base

pkgdir="$1"
p="${2:-$1}"

cd "$pkgdir"

pkgname=$(sed -ne 's/Package: //p' ./DESCRIPTION)

mkdir -p $PWD/repro-test/$p/usr/lib/R/site-library
rm -f ./repro-test/$p/usr/lib/R/site-library/$pkgname/R/$pkgname.rdx
rm -f ./repro-test/$p/usr/lib/R/site-library/$pkgname/R/$pkgname.rdb

install -l repro-test/$p/usr/lib/R/site-library -d . \
	"--built-timestamp=\"Thu, 01 Jan 1970 00:00:00 +0000\""

stat ./repro-test/$p/usr/lib/R/site-library/$pkgname/R/$pkgname.rdx
sha256sum ./repro-test/$p/usr/lib/R/site-library/$pkgname/R/$pkgname.rdx
sha256sum ./repro-test/$p/usr/lib/R/site-library/$pkgname/R/$pkgname.rdb
sha256sum ./repro-test/$p/usr/lib/R/site-library/$pkgname/help/$pkgname.rdx
sha256sum ./repro-test/$p/usr/lib/R/site-library/$pkgname/help/$pkgname.rdb
sha256sum ./repro-test/$p/usr/lib/R/site-library/$pkgname/help/paths.rds
echo "don't forget to rm -rf ${pkgdir}/repro-test/${p} if you want a clean directory"
