#!/bin/bash

# Copyright 2016-2017 Holger Levsen <holger@layer-acht.org>
# released under the GPLv=2

set -e

DRYRUN=false
if [ "$1" = "-d" ] || [ "$1" = "--dry-run" ] ; then
	DRYRUN=true
	shift
fi
EDIT=false
if [ "$1" = "-e" ] || [ "$1" = "--edit-tags" ] ; then
	EDIT=true
	shift
fi

if [ "$1" = "" ] ; then
	echo "need a post number to act on, eg 53"
	exit 1
fi
WEEK=$1
DRAFT=./drafts/$WEEK.mdwn
POST=./posts/$WEEK.mdwn


if [ ! -f $DRAFT ] ; then
	echo "$DRAFT does not exist, exiting."
	exit 1
fi

if [ ! -f ./drafts/$WEEK.tags ] ; then
	# these are grep'ed for case sensitive
	KNOWN_TAGS="ArchLinux Athens buildinfo build_kernel_architecture build_locale cdbs clang Clang coreboot DebConf16 debhelper debugging diffoscope disorderfs documentation dpkg Fedora FreeBSD GCC gcc GSoC gzip installation jenkins.debian.net libxslt live_media MacPorts maven NetBSD OpenWrt ordering Outreachy patches popcon project_infrastructure python reprotest sbuild source_date_epoch SOURCE_DATE_EPOCH sphinx strip-nondeterminism Test_infrastructure test_infrastructure txt2man Ubuntu umask openSUSE tails LEDE F-Droid"
	# these "special" words needs to be separated by word boundaries (and are also case sensitive…)
	KNOWN_SPECIAL_TAGS="dak luatex pdftex R Rust Tar tar TAILS texlive-bin TeX tex Qt"
	# all posts are tagged:
	MY_TAGS="reproducible_builds Debian"

	for TAG in $KNOWN_TAGS ; do
		if [ -n "$(tr '\n' ' ' <$DRAFT | grep "${TAG/_/[_ ]}" 2>/dev/null)" ] ; then
			MY_TAGS="$MY_TAGS $TAG"
		fi
	done
	for TAG in $KNOWN_SPECIAL_TAGS ; do
		if [ -n "$(tr '\n' ' ' <$DRAFT | grep "\\b${TAG}\\b" 2>/dev/null)" ] ; then
			MY_TAGS="$MY_TAGS $TAG"
		fi
	done

	# force capitalisation as we want it
	MY_FINAL_TAGS=""
	for TAG in $MY_TAGS ; do
		if [ "$TAG" = "Tar" ] ; then
			MY_FINAL_TAGS="$MY_FINAL_TAGS tar"
		elif [ "$TAG" = "clang" ] ; then
			MY_FINAL_TAGS="$MY_FINAL_TAGS Clang"
		elif [ "$TAG" = "gcc" ] ; then
			MY_FINAL_TAGS="$MY_FINAL_TAGS GCC"
		else
			MY_FINAL_TAGS="$MY_FINAL_TAGS $TAG"
		fi
	done
	# echo is used to remove the prefixing whitespace
	MY_TAGS=$(echo $MY_FINAL_TAGS)

	if ! $DRYRUN ; then
		echo "Creating $WEEK.tags, please review detected tags:"
		echo "  $MY_TAGS"
		echo $MY_TAGS > ./drafts/$WEEK.tags
	else
		echo "Tags guessed: $MY_TAGS"
	fi
fi

if [ -f $POST ] ; then
	echo "$POST already exists, doing nothing."
	exit 0
elif [ ! -f $DRAFT ] ; then
	echo "$DRAFT does not exist, exiting."
	exit 1
elif [ -z "$(grep 'meta title=' $DRAFT 2>/dev/null)" ] ; then
	echo "$DRAFT has no title, exiting."
	exit 1
elif $DRYRUN ; then
	echo
	echo "Please review the guessed tags."
	exit 0
elif grep -qs FIXME $DRAFT ; then
	echo "$DRAFT contains FIXME statements; refusing to publish."
	exit 1
fi

if $EDIT ; then
	sensible-editor ./drafts/$WEEK.tags
fi

META_TAGS='[[!tag '$(cat ./drafts/$WEEK.tags)']]'
META_DATE='[[!meta date="'$(TZ=UTC LANG=C date +'%a %b %d %H:%M:%S %Y %z')'"]]'

TMPFILE=$(mktemp)
grep "meta title=" $DRAFT > $TMPFILE
echo $META_DATE >> $TMPFILE
echo $META_TAGS >> $TMPFILE
grep -v "meta title=" $DRAFT >> $TMPFILE
cp $TMPFILE $DRAFT
rm $TMPFILE

git rm ./drafts/$WEEK.tags 2>/dev/null || rm ./drafts/$WEEK.tags
git mv $DRAFT $POST
git add $POST
git commit -m "published as https://reproducible.alioth.debian.org/blog/posts/$WEEK/"
echo
git log -1
git tag -s $WEEK -m "Publish week $WEEK"
echo
echo "Now verify the results and run git push && git push --tags."
echo
echo "After publishing, Tweet this via:"
echo
echo "  %tweet What happened in the @ReproBuilds effort between $(tr '\n' ' ' < $POST | sed -n -e 's@.* effort between \([^:]*\):.*@\1@p'): https://reproducible.alioth.debian.org/blog/posts/$WEEK/"
