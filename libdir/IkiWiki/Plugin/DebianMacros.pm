#!/usr/bin/perl
#
# Copyright © 2016 Mattia Rizzolo <mattia@debian.org>
# License: WTFPLv2

package IkiWiki::Plugin::DebianMacros;

use warnings;
use strict;
use IkiWiki 3.00;

sub import {
    hook(type => "preprocess", id => "pkg", call => \&pkg);
    hook(type => "preprocess", id => "bug", call => \&bug);
    hook(type => "preprocess", id => "patch", call => \&patch);
    hook(type => "preprocess", id => "issue", call => \&issue);
    hook(type => "preprocess", id => "pkgset", call => \&pkgset);
}

sub pkg {
    my $pkg = shift;
    my $title = $pkg;
    if (defined $_{title}) {
        $title = $_{title};
    }
    return "[$pkg](https://tracker.debian.org/pkg/$pkg)";
}

sub bug_out {
    my ($bug, $title) = @_;
    unless ($bug =~ /^\d+$/) {
        error("The bug $bug is not a valid bug format.");
    }
    return "[$title](https://bugs.debian.org/$bug)";
}

sub bug {
    my $bug = shift;
    return bug_out($bug, "#$bug");
}

sub patch {
    my $bug = shift;
    return bug_out($bug, "original patch");
}

sub issue {
    my $issue = shift;
    return "[$issue](https://tests.reproducible-builds.org/issues/unstable/${issue}_issue.html)";
}

sub pkgset {
    my $pkgset = shift;
    return "[$pkgset](https://tests.reproducible-builds.org/debian/unstable/amd64/pkg_set_${pkgset}.html)";
}

1;
