[[!meta title="Reproducible Builds: week 74 in Stretch cycle"]]
[[!meta date="Mon Sep 26 21:25:12 2016 +0000"]]
[[!tag reproducible_builds Debian Athens buildinfo diffoscope documentation dpkg ordering Outreachy python reprotest strip-nondeterminism tails F-Droid]]

Here is what happened in the [Reproducible
Builds](https://wiki.debian.org/ReproducibleBuilds) effort between Sunday September 18 and Saturday September 24 2016:

Outreachy
---------

We intend to participate in [Outreachy Round 13](https://wiki.debian.org/Outreachy/Round13/Projects/ReproducibleBuildsOfDebian) and look forward for new enthusiastic applications to contribute to reproducible builds. We're offering four different areas to work on:

- Improve test and debugging tools.
- Improving reproducibility of Debian packages.
- Improving Debian infrastructure.
- Help collaboration across distributions.

Reproducible Builds World summit #2 
-----------------------------------

We are planning e a similar event to our [Athens 2015 summit](https://reproducible-builds.org/events/athens2015/) and expect to reveal more information soon. If you haven't been contacted yet but would like to attend, please contact [holger](mailto:holger@layer-acht.org).

Toolchain development and fixes
-------------------------------

Mattia uploaded [[!pkg dpkg]]/1.18.10.0~reproducible1 to our [experimental repository](https://wiki.debian.org/ReproducibleBuilds/ExperimentalToolchain).
and [covered the details for the upload](https://lists.alioth.debian.org/pipermail/reproducible-builds/Week-of-Mon-20160919/007119.html) in a mailing list post.

The most important change is the incorporation of improvements made by Guillem
Jover (dpkg maintainer) to the .buildinfo generator. This is also in the hope
that it will speed up the merge in the upstream.

One of the other relevant changes from before is that .buildinfo files
generated from binary-only builds will no longer include the hash of the .dsc
file in Checksums-Sha256 as documented in the
[specification](https://wiki.debian.org/ReproducibleBuilds/BuildinfoSpecification#buildinfo_field_descriptions).

Even if it was considered important to include a checksum of the source package
in .buildinfo, storing it that way breaks other assumptions (eg. that
Checksums-Sha256 contains only files part of that are part of a single upload,
whereas the .dsc might not be part of that upload), thus we look forward for
another solution to store the source checksum in .buildinfo.

Bugs filed
----------

* [[!bug 838713]] filed against [[!pkg python-xlib]] by Chris Lamb.
* [[!bug 838754]] filed against [[!pkg golang-google-grpc]] by Chris Lamb.
* [[!bug 838188]] filed against [[!pkg ocaml]] by Johannes Schauer.
* [[!bug 838785]] filed against [[!pkg funnelweb]] by Reiner Herrmann.

Reviews of unreproducible packages
----------------------------------

250 package reviews have been added, 4 have been updated and 4 have been removed in this week,
adding to our knowledge about [identified issues](https://tests.reproducible-builds.org/debian/index_issues.html).

4 issue types have been added:

- [captures\_users\_gecos issue](https://anonscm.debian.org/git/reproducible/notes.git/commit/?id=584deb2)
- [timestamps\_in\_org\_mode\_html\_output toolchain issue.](https://anonscm.debian.org/git/reproducible/notes.git/commit/?id=f9da49e)
- [varnish\_vmodtool\_random\_file\_id](https://anonscm.debian.org/git/reproducible/notes.git/commit/?id=f269ff7)
- [gpg\_keyring\_magic\_bytes\_differ](https://anonscm.debian.org/git/reproducible/notes.git/commit/?id=bf419e3)

3 issue types have been updated:

- [timestamps\_in\_org\_mode\_html\_output more generally.](https://anonscm.debian.org/git/reproducible/notes.git/commit/?id=5b2aaa1)
- [timestamps\_in\_documentation\_generated\_by\_texi2html](https://anonscm.debian.org/git/reproducible/notes.git/commit/?id=2422954)
- [randomness\_in\_ocaml\_preprocessed\_files](https://anonscm.debian.org/git/reproducible/notes.git/commit/?id=65250b4)

Weekly QA work
--------------

FTBFS bugs have been reported by:

 - Chris Lamb (11)
 - Santiago Vila (2)

Documentation updates
---------------------

h01ger created a new Jenkins job so that every commit pushed to the [master branch for the website](https://git.debian.org/git/reproducible/reproducible-website.git) will update [reproducible-builds.org](https://reproducible-builds.org/).


diffoscope development
----------------------

- Mattia Rizzolo:
  - [Skip rlib tests if the "nm" tool is missing](https://anonscm.debian.org/git/reproducible/diffoscope.git/commit/?id=1d7bea1)
- Ximin Luo:
  - [Give better advice about what envvar to set to make the console work](https://anonscm.debian.org/git/reproducible/diffoscope.git/commit/?id=436253a)
  - [tests/basic-command-line: check exit code and use a more complex example](https://anonscm.debian.org/git/reproducible/diffoscope.git/commit/?id=7adb6b7)
  - [Add a script to check sizes of dependencies](https://anonscm.debian.org/git/reproducible/diffoscope.git/commit/?id=30d00ec)
  - [Don't use unicode quotes to avoid breakage under LC\_ALL=C](https://anonscm.debian.org/git/reproducible/diffoscope.git/commit/?id=f68fcbd)


strip-nondeterminism development
--------------------------------

- Chris Lamb:
  - [.perltidyrc: Add from lintian.](https://anonscm.debian.org/git/reproducible/strip-nondeterminism.git/commit/?id=dcddf05)
  - [.perltidyrc: We use tabs, not spaces.](https://anonscm.debian.org/git/reproducible/strip-nondeterminism.git/commit/?id=c419282)
  - [Run perltidy](https://anonscm.debian.org/git/reproducible/strip-nondeterminism.git/commit/?id=d1126e4)

reprotest development
---------------------

- Ximin Luo uploaded reprotest 0.3 and 0.3.1 to unstable with these changes:
  - [Make some variations more reliable, so tests don't fail](https://anonscm.debian.org/git/reproducible/reprotest.git/commit/?id=4797f65)
  - [Add a safety device to guard against typos](https://anonscm.debian.org/git/reproducible/reprotest.git/commit/?id=c0017ce)
  - [Address lintian warnings](https://anonscm.debian.org/git/reproducible/reprotest.git/commit/?id=ec4db94)
  - [Remove any existing artifact, in case the build script doesn't overwrite it](https://anonscm.debian.org/git/reproducible/reprotest.git/commit/?id=6b25439)
  - [Fix the logic of some tests, and don't vary fileordering on Debian buildds](https://anonscm.debian.org/git/reproducible/reprotest.git/commit/?id=0b0e6dc)
  - [Use the magic of VIRTUALENV\_DOWNLOAD=no, seen in tox's own autopkgtest tests](https://anonscm.debian.org/git/reproducible/reprotest.git/commit/?id=fe49513)
  - [Flush so subprocess output is guaranteed to appear later](https://anonscm.debian.org/git/reproducible/reprotest.git/commit/?id=c1b17ed)
  - [Don't error if the build command generates stderr](https://anonscm.debian.org/git/reproducible/reprotest.git/commit/?id=49e67ca)
  - [Default tests to run on "null" only since it takes effort to set up the others](https://anonscm.debian.org/git/reproducible/reprotest.git/commit/?id=07ec6d1)
  - [hey dawg i herd u liek tests so i put some tests in ur tests so u can test while u test](https://anonscm.debian.org/git/reproducible/reprotest.git/commit/?id=28937c1)
  - [Make no\_clear\_on\_error optional; we don't want to pass it in everywhere e.g. tests](https://anonscm.debian.org/git/reproducible/reprotest.git/commit/?id=2ea325f)
  - [Output a nice big obvious summary at the end when successful](https://anonscm.debian.org/git/reproducible/reprotest.git/commit/?id=77219d8)
  - [Don't repeat documentation in two different places, move it all to --help](https://anonscm.debian.org/git/reproducible/reprotest.git/commit/?id=714cf5b)
  - [More reliable build artifact pattern matching, and update docs](https://anonscm.debian.org/git/reproducible/reprotest.git/commit/?id=eb4146f)
  - [Use a list comprehension for slightly more idiomatic python than map/lambda](https://anonscm.debian.org/git/reproducible/reprotest.git/commit/?id=0876afc)
  - [Make multi-component artifact patterns like '*.deb *.changes' work correctly](https://anonscm.debian.org/git/reproducible/reprotest.git/commit/?id=225b0e0)
  - [Add a --no-clean-on-error option so you can analyse what went wrong](https://anonscm.debian.org/git/reproducible/reprotest.git/commit/?id=56d4ca3)
  - [More help text for virtual\_server\_args](https://anonscm.debian.org/git/reproducible/reprotest.git/commit/?id=6bddf77)
  - [Support shell patterns as the build\_artifact](https://anonscm.debian.org/git/reproducible/reprotest.git/commit/?id=c904142)
  - [Use sys.exit inside main(), not check() as it's more idiomatic](https://anonscm.debian.org/git/reproducible/reprotest.git/commit/?id=5356853)
  - [Pass kind='build' to check\_exec so it doesn't time out after 100s](https://anonscm.debian.org/git/reproducible/reprotest.git/commit/?id=02119cb)
  - [adt\_testbed: add stdout/stderr to the "auxverb failed" error message, if available](https://anonscm.debian.org/git/reproducible/reprotest.git/commit/?id=44d6ffc)
  - [If no virtual\_server is given then use "null"](https://anonscm.debian.org/git/reproducible/reprotest.git/commit/?id=4a08037)


tests.reproducible-builds.org
-----------------------------

- The full rebuild of all packages in unstable (for all tested archs) with the new build path variation has been completed. This has had the result that we are down to ~75% reproducible packages in unstable now. In comparison, for testing (where we don't vary the build path) we are still at ~90%. IRC notifications for unstable have been enabled again. (Holger)
- Make the notes job robust about bad data (see [[!bug 833695]] and [[!bug 833738]]). (Holger)
- Setup profitbricks-build7 running stretch as testing reproducible builds of F-Droid need to use a newer version of vagrant in order to support running vagrant VMs with kvm on kvm. (Holger)
- The misbehaving 'opi2a' armhf node has been replaced with a Jetson-TK1 board kindly donated by NVidia. This machine is using an NVIDIA tegra-k1 (cortex-a15) quad-core board. (vagrant and Holger)

Misc.
-----

This week's edition was written by Chris Lamb, Holger Levsen and Mattia Rizzolo and reviewed by a bunch of Reproducible Builds folks on IRC.
