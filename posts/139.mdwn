[[!meta title="Reproducible Builds: Weekly report #139"]]
[[!meta date="Thu Dec 28 12:55:15 2017 +0000"]]
[[!tag reproducible_builds Debian diffoscope disorderfs jenkins.debian.net reprotest SOURCE_DATE_EPOCH strip-nondeterminism Ubuntu openSUSE]]

Here's what happened in the [Reproducible
Builds](https://reproducible-builds.org) effort between Sunday December 17 and
Saturday December 23 2017:


Packages reviewed and fixed, and bugs filed
-------------------------------------------

Bugs filed in Debian:

* Chris Lamb:
    * [[!bug 884677]] filed against [[!pkg bitseq]].
    * [[!bug 884714]] filed against [[!pkg gtranslator]].
    * [[!bug 884936]] filed against [[!pkg nanoc]].
    * [[!bug 885063]] filed against [[!pkg cairomm]].

Bugs filed in openSUSE:

* Bernhard M. Wiedemann:
  * [WindowMaker](https://build.opensuse.org/request/show/558817) (merged) -
    use modification date of ChangeLog, upstreamable
  * [ntp](https://build.opensuse.org/request/show/559102) (merged) - drop date
  * [bzflag](https://build.opensuse.org/request/show/559126) - version upgrade
    to include already-upstreamed `SOURCE_DATE_EPOCH` patch


Reviews of unreproducible packages
----------------------------------

20 package reviews have been added, 36 have been updated and 32 have been removed in this week,
adding to our knowledge about [identified issues](https://tests.reproducible-builds.org/debian/index_issues.html).


Weekly QA work
--------------

During our reproducibility testing, FTBFS bugs have been detected and reported by:

 - Adrian Bunk (6)
 - Matthias Klose (8)


diffoscope development
----------------------

- Juliana Oliveira Rodrigues:
    - [tests: comparators: test\_rlib: fixes llvm >= 5.0 test case](https://anonscm.debian.org/git/reproducible/diffoscope.git/commit/?id=f4e50d7)
- Chris Lamb:
    - [Support Android ROM boot.img introspection. (Closes: #884557)](https://anonscm.debian.org/git/reproducible/diffoscope.git/commit/?id=18364f4)
- Holger Levsen:
    - [Update Standards-Version to 4.1.2. No changes required.](https://anonscm.debian.org/git/reproducible/diffoscope.git/commit/?id=f8adedc)


strip-nondeterminism development
--------------------------------

- Holger Levsen:
    - [Update Standards-Version to 4.1.2. No changes required.](https://anonscm.debian.org/git/reproducible/strip-nondeterminism.git/commit/?id=c86804f)


disorderfs development
----------------------

- Holger Levsen:
    - [Update Standards-Version to 4.1.2. No changes required.](https://anonscm.debian.org/git/reproducible/disorderfs.git/commit/?id=e75793e)
    - [d/control: Declare that disorderfs doesn't need root to build.](https://anonscm.debian.org/git/reproducible/disorderfs.git/commit/?id=679c675)


reprotest development
---------------------

- Holger Levsen:
    - [d/control: Declare that strip-nondeterminism doesn't need root to build.](https://anonscm.debian.org/git/reproducible/reprotest.git/commit/?id=1836b1f)


reproducible-website development
--------------------------------

- Chris Lamb:
  - rws3:
    - Huge number of formatting improvements, typo fixes, capitalisation
    - Add section headings to make splitting up easier.
- Holger Levsen:
  - rws3:
    - Add a disclaimer that this part of the website is a Work-In-Progress.
    - Split notes from each session into separate pages (6 sessions).
    - Other formatting and style fixes.
    - Link to Ludovic Courtès' [notes on GNU
	  Guix](https://www.gnu.org/software/guix/blog/2017/reproducible-builds-a-status-update/).
- Ximin Luo:
  - rws3:
    - Format agenda.md to look like previous years', and other fixes
    - Split notes from each session into separate pages (1 session).


jenkins.debian.net development
------------------------------

- Hans-Christoph Steiner:
    - [reproducible fdroid: update cleanup\_all to kill random daemons leftover](https://anonscm.debian.org/git/qa/jenkins.debian.net.git/commit/?id=874ff3e9)
- Holger Levsen:
    - [make dsa-check-running-kernel work with changes in Ubuntu LTS. WIP, not yet deployed to all the other hosts](https://anonscm.debian.org/git/qa/jenkins.debian.net.git/commit/?id=dd9b5305)


Misc.
-----

This week's edition was written by Ximin Luo and Bernhard M. Wiedemann &
reviewed by a bunch of Reproducible Builds folks on IRC & the mailing lists.
