[[!meta title="Reproducible Builds: Weekly report #126"]]
[[!meta date="Tue Sep 26 07:22:44 2017 +0000"]]
[[!tag reproducible_builds Debian debugging diffoscope NetBSD reprotest SOURCE_DATE_EPOCH strip-nondeterminism umask tails]]

Here's what happened in the [Reproducible Builds](https://reproducible-builds.org) effort between Sunday September 17th and Saturday September 23rd 2017:

Media coverage
--------------

- Christos Zoulas gave a talk entitled *Reproducible builds on NetBSD* at [EuroBSDCon 2017](https://2017.eurobsdcon.org/talk-speakers/)

Reproducible work in other packages
-----------------------------------

- Paul Eggert [reported that TZ=UTC is not a portable setting for the TZ environment variable](http://lists.alioth.debian.org/pipermail/reproducible-builds/Week-of-Mon-20170918/009289.html).

Packages reviewed and fixed, and bugs filed
-------------------------------------------

* Adrian Bunk:
  * [[!bug 876615]] filed against [[!pkg librsvg]].

* Bernhard M. Wiedemann:
  * [varnish](https://github.com/varnishcache/varnish-cache/pull/2436) (random IDs)
  * [make](https://savannah.gnu.org/bugs/index.php?52076) (sort)
  * [nautilus-dropbox](https://github.com/dropbox/nautilus-dropbox/pull/31) (extended date)
  * [fontforge](https://github.com/fontforge/fontforge/pull/3152) (date)
  * [votca-csg](https://github.com/votca/csg/pull/228) (merged, date)
  * [freeipmi](https://savannah.gnu.org/patch/index.php?9457) (merged, date)
  * [libmypaint](https://github.com/mypaint/libmypaint/pull/108) (merged, sort)
  * [doomsday](https://github.com/skyjake/Doomsday-Engine/pull/18) (merged, sort)
  * [asciidoc](https://github.com/asciidoc/asciidoc/pull/115) (help ``make release`` with [SOURCE_DATE_EPOCH](https://reproducible-builds.org/specs/source-date-epoch/) patch)


Reviews of unreproducible packages
----------------------------------

1 package reviews was added, 49 have been updated and 54 have been removed in this week,
adding to our knowledge about [identified issues](https://tests.reproducible-builds.org/debian/index_issues.html).

One issue type was updated:

- [gtk\_doc\_api\_index\_full](https://anonscm.debian.org/git/reproducible/notes.git/commit/?id=2f1ecb12) (fixed upstream: [[!bug 779090]])

Weekly QA work
--------------

During our reproducibility testing, FTBFS bugs have been detected and reported by:

 - Adrian Bunk (56)
 - Bas Couwenberg (1)
 - Helmut Grohne (1)
 - Nobuhiro Iwamatsu (2)


diffoscope development
----------------------

Version 87 was uploaded to unstable by Mattia Rizzolo. It included [contributions](https://anonscm.debian.org/git/reproducible/diffoscope.git/log/?h=debian/87) from:

- Ximin Luo:
    - ``comparators/*``:
        - [Add a test for fallback\_recognizes and improve the behaviour](https://anonscm.debian.org/git/reproducible/diffoscope.git/commit/?id=27be3f4)
        - [Add a fallback\_recognizes to work around file(1) #876316.](https://anonscm.debian.org/git/reproducible/diffoscope.git/commit/?id=7b8b9ae) (Closes: [[!bug 875272]])
        - [If --force-details then don't skip files with identical md5sums either](https://anonscm.debian.org/git/reproducible/diffoscope.git/commit/?id=9b87bd4)
        - [Add a --force-details flag for debugging](https://anonscm.debian.org/git/reproducible/diffoscope.git/commit/?id=8ab261e)
    - ``presenters.html``:
        - [Restore the previous more-detailed comment](https://anonscm.debian.org/git/reproducible/diffoscope.git/commit/?id=21f931a)
        - [Don't show pointer-cursor when jQuery is disabled](https://anonscm.debian.org/git/reproducible/diffoscope.git/commit/?id=48ba0aa)
        - [Prune all descendants properly](https://anonscm.debian.org/git/reproducible/diffoscope.git/commit/?id=ccd926f) (Closes: [[!bug 875281]])
    - ``difference.py``:
        - [Also copy self.\_comment properly, compare self.\_visuals in equals()](https://anonscm.debian.org/git/reproducible/diffoscope.git/commit/?id=f5c9986)
        - [In fmap/map\_lines, don't forget about self.\_visuals](https://anonscm.debian.org/git/reproducible/diffoscope.git/commit/?id=3a8ab73)
        - [Use diff\_split\_lines everywhere](https://anonscm.debian.org/git/reproducible/diffoscope.git/commit/?id=a9bae3a)
    - tests:
        - [Add case for #875281](https://anonscm.debian.org/git/reproducible/diffoscope.git/commit/?id=4cbca96)
        - [Make test\_md5sums less brittle](https://anonscm.debian.org/git/reproducible/diffoscope.git/commit/?id=0fb6d8a)
        - [Update test\_deb for new string](https://anonscm.debian.org/git/reproducible/diffoscope.git/commit/?id=f5e9215)
    - [readers: Convert bytes to str in the right place](https://anonscm.debian.org/git/reproducible/diffoscope.git/commit/?id=8c92a26)
    - [config: Force-set a value if it must be < another and it was not set on purpose](https://anonscm.debian.org/git/reproducible/diffoscope.git/commit/?id=510162b) (Closes: [[!bug 875451]])
    - [Bump minimum Python version to 3.5 as we use syntax introduced by PEP 448](https://anonscm.debian.org/git/reproducible/diffoscope.git/commit/?id=ad8ab31)
- Chris Lamb:
    - [Print a debugging message if we are reading diff from stdin.](https://anonscm.debian.org/git/reproducible/diffoscope.git/commit/?id=509509e)
    - [Compare types with identity not equality.](https://anonscm.debian.org/git/reproducible/diffoscope.git/commit/?id=98de493)
    - [diffoscope.presenters.html: Use logging.py's lazy argument interpolation.](https://anonscm.debian.org/git/reproducible/diffoscope.git/commit/?id=6e42152)
    - [debian/control: Bump Standards-Version to 4.1.0.](https://anonscm.debian.org/git/reproducible/diffoscope.git/commit/?id=c561ae5)
- Mattia Rizzolo:
    - [debian/rules: Place "before" commands before "after" commands.](https://anonscm.debian.org/git/reproducible/diffoscope.git/commit/?id=e07585a)


strip-nondeterminism development
--------------------------------

- Chris Lamb:
  - [Log which handler procesed a file.](https://anonscm.debian.org/git/reproducible/strip-nondeterminism.git/commit/?id=aa9c311) (Closes: [[!bug 876140]])
  - [Bump Standards-Version to 4.1.0.](https://anonscm.debian.org/git/reproducible/strip-nondeterminism.git/commit/?id=d17ee5b)


reprotest development
---------------------

Version 0.7 was uploaded to unstable by Ximin Luo:

- Ximin Luo:
  - [Push use of UNIX return codes to the edges of the program](https://anonscm.debian.org/git/reproducible/reprotest.git/commit/?id=1f93944)
  - [Add a --auto-build option to determine which variations cause unreproducibility](https://anonscm.debian.org/git/reproducible/reprotest.git/commit/?id=fd4a053)
  - [Allow umask and user\_group to both vary at the same time](https://anonscm.debian.org/git/reproducible/reprotest.git/commit/?id=27c1391)
  - [Generate build names in main instead of build, guard against dupes](https://anonscm.debian.org/git/reproducible/reprotest.git/commit/?id=5ad0200)
  - [Pull traceback-printing stuff out of the core code](https://anonscm.debian.org/git/reproducible/reprotest.git/commit/?id=8b03fcf)
  - [More refactoring, make check() contain only logic that would be changed in an auto-detector](https://anonscm.debian.org/git/reproducible/reprotest.git/commit/?id=374c580)
  - [Split check() into a coroutine producer and consumer, prepares auto-detection](https://anonscm.debian.org/git/reproducible/reprotest.git/commit/?id=f4b5e84)


tests.reproducible-builds.org
-----------------------------

Vagrant Cascadian and Holger Levsen:

- Re-add and `armhf` build node that had been disabled due to
  performance issues, but works linux 4.14-rc1 now! [[!bug 876212]]

Holger Levsen:

- Use [botch](https://tracker.debian.org/botch) from stretch to fix the [jenkins job which create the package sets](https://jenkins.debian.net/job/reproducible_create_meta_pkg_sets/). (botch is currently uninstallable in sid and from pre-stretch-release times we used a sid schroot to install and use botch.)

Misc.
-----

This week's edition was written by Bernhard M. Wiedemann, Chris Lamb, Vagrant Cascadian & reviewed by a bunch of Reproducible Builds folks on IRC & the mailing lists.
