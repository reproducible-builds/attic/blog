[[!meta title="Reprotest containers are (probably) working, finally"]]
[[!meta date="Wed, 13 Jul 2016 11:49:16 -0400"]]
[[!tag reproducible_builds Debian reprotest Outreachy]]
Author: ceridwen

After testing and looking at the code for
[Plumbum](https://plumbum.readthedocs.io/), I decided that it wouldn't
work for my purposes.  When a command is created by something like
`remote['ls']`, it actually looks up which `ls` executable will be run
and uses in the command object an internal representation of a path
like `/bin/ls`.  To make it work with autopkgtest's code would have
required writing some kind of middle layer that would take all of the
Plumbum code that makes subprocess calls, does path lookups, or uses
the Python standard library to access OS functions and convert them
into shell scripts.  Another similar library,
[sh](https://amoffat.github.io/sh/), has the same problems.  I think
there's a strong argument that something like Plumbum's or sh's API
would be much easier to work with than adt_testbed, and I may take
steps to improve it at some point, but for the moment I've focused on
getting reprotest working with the existing autopkgtest/adt_testbed
API.

To do this, I created a minimalistic shell AST library in
`_shell_ast.py` using the [POSIX shell
grammar](http://pubs.opengroup.org/onlinepubs/9699919799/utilities/V3_chap02.html#tag_18_10).
I omitted a lot of functionality that wasn't immediately relevant for
reprotest and simplified the AST in some places to make it easier to
work.  Using this, it generates a shell script and runs it with
`adt_testbed.Testbed.execute()`.  With these pieces in place, the
tests succeed for both null and schroot!  I haven't tested the rest of
the containers, but barring further issues with autopkgtest, I expect
they should work.

At this point, my goal is to push the next release out by next week,
though as usual it will depend on how many snags I hit in the process.
I see the following as the remaining blockers:

* Test chroot and qemu in addition to null and schroot.

* PyPi still doesn't install the scripts in `virt/` properly.

* While I fixed [part
  of](https://anonscm.debian.org/cgit/reproducible/reprotest.git/commit/?h=virtualization&id=8f1fe119fd3c7fdf10bace47aa8362d64d4456db)
  adt_testbed's error handling, some build failures still cause it to
  hang, and I have to kill the process by hand.

* Better user documentation.  I don't have time to be thorough, but I
  can provide a few more pointers.
