[[!meta title="Reproducible Builds: Weekly report #146"]]
[[!meta date="Tue Feb 13 18:00:15 2018 +0000"]]
[[!tag reproducible_builds Debian debugging diffoscope disorderfs jenkins.debian.net NetBSD Outreachy]]

Here's what happened in the [Reproducible Builds](https://reproducible-builds.org) effort between Sunday February 4 and Saturday February 10 2018:

Media coverage
--------------

* On Febuary 7th, Christos Zoulas gave at talk entitled [Reproducible builds on NetBSD](http://www.nycbug.org/index.cgi?action=view&id=10657) at the [New York City \*BSD User Group](http://www.nycbug.org/).


Packages reviewed and fixed, and bugs filed
-------------------------------------------

* Chris Lamb:
    * [[!bug 889565]] filed against [[!pkg libical3]].
    * [[!bug 889637]] filed against [[!pkg mailman]].
    * [[!bug 890027]] filed against [[!pkg librsvg]] ([forwarded upstream](https://gitlab.gnome.org/GNOME/librsvg/merge_requests/10)).
    * [[!bug 890036]] filed against [[!pkg juce]].
    * [[!bug 890052]] filed against [[!pkg nsf]].
    * [[!bug 890053]] filed against [[!pkg tkmpeg]].
    * [[!bug 885408]] (re-filed) against [[!pkg multipath-tools]].


Reviews of unreproducible packages
----------------------------------

63 package reviews have been added, 26 have been updated and 19 have been removed in this week,
adding to our knowledge about [identified issues](https://tests.reproducible-builds.org/debian/index_issues.html).

A new issue type have been added:

* [`timestamps_in_pdf_generated_by_rsvg_convert`](https://anonscm.debian.org/git/reproducible/notes.git/commit/?id=5e8bac75)

Weekly QA work
--------------

During our reproducibility testing, FTBFS bugs have been detected and reported by:

 - Adrian Bunk (34)
 - Antonio Terceiro (1)
 - James Cowgill (1)
 - Matthias Klose (1)


diffoscope development
----------------------

- Chris Lamb:
    - [Add support for comparing binary XML schemas](https://anonscm.debian.org/git/reproducible/diffoscope.git/commit/?id=c1e29ac)
    - [Drop debugging code in Android tests](https://anonscm.debian.org/git/reproducible/diffoscope.git/commit/?id=2bf2f0f)
- Mattia Rizzolo:
    - [Wrap some lines](https://anonscm.debian.org/git/reproducible/diffoscope.git/commit/?id=b7332b6)

In addition, Juliana—our Outreachy intern—continues her work on parallel processing.


disorderfs development
----------------------

- Chris Lamb:
    - [Override `no-upstream-changelog` lintian warning](https://anonscm.debian.org/git/reproducible/disorderfs.git/commit/?id=e98218f)
    - [Use HTTPS format URI in `debian/copyright`](https://anonscm.debian.org/git/reproducible/disorderfs.git/commit/?id=cb394cd)


jenkins.debian.net development
------------------------------

- Mattia Rizzolo:
    - [Correct reference to IRC channel](https://anonscm.debian.org/git/qa/jenkins.debian.net.git/commit/?id=8eb3c2d1)
    - [Update link to NetBSDs `src` Git repository](https://anonscm.debian.org/git/qa/jenkins.debian.net.git/commit/?id=a13fee6a)


Misc.
-----

This week's edition was written by Chris Lamb, Holger Levsen & reviewed by a bunch of Reproducible Builds folks on IRC & the mailing lists.




