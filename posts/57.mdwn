[[!meta title="Reproducible builds: week 57 in Stretch cycle"]]
[[!meta date="Mon May 30 21:57:43 2016 +0000"]]
[[!tag reproducible_builds Debian documentation installation Outreachy SOURCE_DATE_EPOCH tar]]

What happened in the [Reproducible
Builds](https://wiki.debian.org/ReproducibleBuilds) effort between May 22nd and May 28th 2016:

Media coverage
--------------

 * Holger Levsen was invited to [RIPE72](https://ripe72.ripe.net) to talk about [Reproducible Builds and a hope for a more secure future](https://ripe72.ripe.net/archive/video/Holger_Levsen-Reproducible_Builds_for_Debian%2C_and_a_Hope_for_a_More_Secure_Future-20160526-095438.mp4) (Video, 25min and [slides](https://ripe72.ripe.net/presentations/158-2016-05-24-ripe72.pdf)) in the [cooperative workgroup](https://ripe72.ripe.net/programme/meeting-plan/coop-wg/).
 * Scarlett Clark blogged about her first week of [Outreachy work](https://www.gnome.org/outreachy/) trying to make packages build reproducible, [describing her efforts with kapptemplate, choqok and kdevplatform](http://scarlettgatelyclark.com/2016/debian-outreachy-debian-reproducible-builds-week-1-progress-report/).

Documentation update
--------------------

 * The wiki page [TimestampsProposal](https://wiki.debian.org/ReproducibleBuilds/TimestampsProposal) has been extended to cover more usage examples and to list more softwares supporting SOURCE_DATE_EPOCH. (Axel Beckert, Dhole and Ximin Luo)
 * h01ger started a [reference card for tools and information about reproducible builds](https://anonscm.debian.org/git/reproducible/reference-card.git) but hasn't progressed much yet. Help with it is much welcome, this is also a good opportunity to learn about this project ;-) The idea is simply to have one coherent place with pointers to all the stuff we have and provide, without repeating nor replacing other documentation.

Toolchain fixes
---------------

 * Alexis Bienvenüe submitted a patch ([[!bug 824050]]) against [[!pkg emacs24]] for `SOURCE_DATE_EPOCH` support in autoloads files, but upstream already [disabled timestamps by default](http://git.savannah.gnu.org/cgit/emacs.git/tree/etc/NEWS#n406) some time before.
 * [[!pkg proj]]/4.9.2-3 uploaded by Bas Couwenberg ([[!patch 825088]] by Alexis Bienvenü) properly initializes memory with zero to prevent the nad2bin tool from leaking random memory content into output artefacts.
 * Reiner Herrmann submitted a patch ([[!bug 825569]], [upstream](https://github.com/ruby/ruby/pull/1367)) against Ruby to sort object files in generated Makefiles, which are used to compile C sources that are part of Ruby projects.

Packages fixed
--------------

The following 18 packages have become reproducible due to changes in their
build dependencies:
[[!pkg canl-c]]
[[!pkg configshell]]
[[!pkg dbus-java]]
[[!pkg dune-common]]
[[!pkg frobby]]
[[!pkg frown]]
[[!pkg installation-guide]]
[[!pkg jexcelapi]]
[[!pkg libjsyntaxpane-java]]
[[!pkg malaga]]
[[!pkg octave-ocs]]
[[!pkg paje.app]]
[[!pkg pd-boids]]
[[!pkg pfstools]]
[[!pkg r-cran-rniftilib]]
[[!pkg scscp-imcce]]
[[!pkg snort]]
[[!pkg vim-addon-manager]]

The following packages have become reproducible after being fixed:

 * [[!pkg apngasm]]/2.7-2 by Manuel A. Fernandez Montecelo, [[!patch 782200]] by Reiner Herrmann.
 * [[!pkg apngdis]]/2.5-2 by Manuel A. Fernandez Montecelo, [[!patch 782197]] by Reiner Herrmann.
 * [[!pkg apngopt]]/1.2-2 by Manuel A. Fernandez Montecelo, [[!patch 782193]] by Reiner Herrmann.
 * [[!pkg asio]]/1:1.10.6-4 by Markus Wanner.
 * [[!pkg bowtie]]/1.1.2-4 by Sascha Steinbiss.
 * [[!pkg bowtie2]]/2.2.9-2 by Sascha Steinbiss.
 * [[!pkg breeze]]/4:5.6.4-1 by Maximiliano Curia, [[!patch 819512]] by Eduard Sanou.
 * [[!pkg cain]]/1.10+dfsg-2 by Sascha Steinbiss.
 * [[!pkg cdist]]/4.0.0-2 by Dmitry Bogatov, [[!patch 825406]] by Chris Lamb.
 * [[!pkg crossroads]]/2.81-1 by Reiner Herrmann.
 * [[!pkg elasticsearch]]/1.7.5-1 by Emmanuel Bourg.
 * [[!pkg gdal]]/2.1.0+dfsg-3 by Bas Couwenberg, [[!patch 824808]] by Alexis Bienvenüe.
 * [[!pkg grass]]/7.0.4-2 by Bas Couwenberg, [[!patch 825092]] by Alexis Bienvenüe.
 * [[!pkg jaligner]]/1.0+dfsg-3 by Michael R. Crusoe.
 * [[!pkg khmer]]/2.0+dfsg-7 by Michael R. Crusoe.
 * [[!pkg libhdf4]]/4.2.11-2 by Bas Couwenberg.
 * [[!pkg libjspeex-java]]/0.9.7-4 by Emmanuel Bourg.
 * [[!pkg libmialm]]/1.0.9-1 by Gert Wollny.
 * [[!pkg libtheora]]/1.1.1+dfsg.1-10 by Petter Reinholdtsen.
 * [[!pkg odil]]/0.6.0-2 by Julien Lamy.
 * [[!pkg pacemaker]]/1.1.15~rc3-1 by Ferenc Wágner.
 * [[!pkg pvm]]/3.4.6-1 by James Clarke.
 * [[!pkg rna-star]]/2.5.2a+dfsg-2 by Sascha Steinbiss.
 * [[!pkg smalt]]/0.7.6-6 by Sascha Steinbiss.
 * [[!pkg soapdenovo2]]/240+dfsg-3 by Sascha Steinbiss.
 * [[!pkg svtplay-dl]]/1.1-1 by Olof Johansson.
 * [[!pkg t-coffee]]/11.00.8cbe486-3 by Sascha Steinbiss.
 * [[!pkg toppred]]/1.10-3 by Sascha Steinbiss.
 * [[!pkg transdecoder]]/3.0.0+dfsg-2 by Michael R. Crusoe.
 * [[!pkg vpim]]/0.695-1.4 by Herbert Parentes Fortes Neto.
 * [[!pkg vtk-dicom]]/0.7.7-2 by Gert Wollny.
 * [[!pkg xplc]]/0.3.13-6 by Reiner Herrmann.

Some uploads have fixed some reproducibility issues, but not all of them:

 * [[!pkg codeblocks]]/16.01+dfsg-1 by Vincent Cheng, [[!patch 824182]] by Fabian Wolff.
 * [[!pkg gmt]]/5.2.1+dfsg-6 by Bas Couwenberg, [[!patch 824668]] by Alexis Bienvenü.

Patches submitted that have not made their way to the archive yet:

 * [[!bug 803547]] against [[!pkg bbswitch]] (reopened) by Reiner Herrmann: sort members of tar archive
 * [[!bug 806945]] against [[!pkg bash]] (follow-up) by Reiner Herrmann: use system man2html instead of embedded copy
 * [[!bug 825122]] against [[!pkg kapptemplate]] by Scarlett Clark: set owner/group of members in tarball to root
 * [[!bug 825138]] against [[!pkg console-setup]] by Reiner Herrmann: fix umask issue; sort entries in shell script; sort fontsets/charmaps locale-independently
 * [[!bug 825285]] against [[!pkg kodi]] by Lukas Rechberger: replace build timestamps with version numbers
 * [[!bug 825322]] against [[!pkg choqok]] by Scarlett Clark: force UTF-8 locale so kconfig_compiler behaves correctly
 * [[!bug 825544]] against [[!pkg wavemon]] by Reiner Herrmann: sort list of object files
 * [[!bug 825545]] against [[!pkg dwm]] by Reiner Herrmann: sort list of header files
 * [[!bug 825547]] against [[!pkg tennix]] by Reiner Herrmann: sort list of data files being archived
 * [[!bug 825584]] against [[!pkg ffmpeg2theora]] by Reiner Herrmann: sort list of source files
 * [[!bug 825588]] against [[!pkg kball]] by Reiner Herrmann: sort list of source files
 * [[!bug 825634]] against [[!pkg miceamaze]] by Reiner Herrmann: sort list of object files
 * [[!bug 825643]] against [[!pkg dash]] by Reiner Herrmann: fix sorting of struct members in generated source file
 * [[!bug 825655]] against [[!pkg libselinux]] by Reiner Herrmann: sort list of source files
 * [[!bug 825656]] against [[!pkg libsepol]] by Reiner Herrmann: sort list of source files
 * [[!bug 825674]] against [[!pkg libsemanage]] by Reiner Herrmann: sort list of source files

Package reviews
---------------

123 reviews have been added, 57 have been updated and 135 have been removed in this week.

 * 5 new issues have been identified:
   * [[!issue timestamps_in_pdf_generated_by_imagemagick]]
   * [[!issue ruby_mkmf_makefile_unsorted_objects]]
   * [[!issue timestamps_from_timestamp_macro]]
   * [[!issue scons_doesnt_pass_environment_to_build_tools]]
   * [[!issue timestamps_from_cpp_macros_in_d]]

21 FTBFS bugs have been reported by Chris Lamb and Santiago Vila.

strip-nondeterminism development
--------------------------------

 * strip-nondeterminsim development: treat *.htb as Zip files (by Sascha Steinbiss).
 * [[!pkg strip-nondeterminism]] 0.017-1 uploaded by h01ger.

tests.reproducible-builds.org
-----------------------

 * The [kde pkg set](https://tests.reproducible-builds.org/unstable/amd64/pkg_set_kde.html) was extended, though the change ain't visible yet, as there are currently non-installable packages in it (and so the set can't be computed). (h01ger)

Misc.
-----

 * Mattia improved [misc.git/reports](https://anonscm.debian.org/cgit/reproducible/misc.git/tree/reports) (=the tools to help writing the weekly statistics for this blog) some more.


This week's edition was written by Reiner Herrmann and Holger Levsen and reviewed by a bunch of Reproducible builds folks on IRC.

