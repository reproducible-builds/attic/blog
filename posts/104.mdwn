[[!meta title="Reproducible Builds: week 104 in Stretch cycle"]]
[[!meta date="Tue Apr 25 07:38:47 2017 +0000"]]
[[!tag reproducible_builds Debian buildinfo diffoscope python]]

Here's what happened in the [Reproducible Builds](https://reproducible-builds.org) effort between Sunday April 16 and Saturday April 22 2017:

Upcoming events
---------------

* On April 26th Chris Lamb will give a talk at [foss-north 2017](http://foss-north.se/) in Gothenburg, Sweden on Reproducible Builds.

* Between May 5th-7th the [Reproducible Builds Hackathon 2017](https://wiki.debian.org/ReproducibleBuilds/HamburgHackathon2017) will take place in Hamburg, Germany.

* On May 13th Chris Lamb will give a talk at [OSCAL'17](https://oscal.openlabs.cc/) in Tirana, Albania on Reproducible Builds.

Reproducible work in other projects
-----------------------------------

* It is [now possible to upload .buildinfo files to Launchpad](https://bugs.launchpad.net/launchpad/+bug/1657704).

* There has been [more progress on rustc reproducible debugging symbols](https://github.com/rust-lang/rust/pull/41419).


Packages reviewed and fixed, and bugs filed
-------------------------------------------

Chris Lamb:

* [[!bug 860470]] filed against [[!pkg libccrtp]].
* [[!bug 860731]] filed against [[!pkg viruskiller]].
* [[!bug 860770]] filed against [[!pkg qjackctl]].
* [[!bug 860848]] filed against [[!pkg osinfo-db]].
* [[!bug 860972]] filed against [[!pkg cyclades-serial-client]].

Chris West:

* [[!bug 860418]] filed against [[!pkg sugar-memorize-activity]]. (Patch by Chris Lamb)


Reviews of unreproducible packages
----------------------------------

37 package reviews have been added, 64 have been updated and 16 have been removed in this week,
adding to our knowledge about [identified issues](https://tests.reproducible-builds.org/debian/index_issues.html).

One issue type has been updated:

- [randomness\_in\_r\_rdb\_rds\_databases: add hint to code line number](https://anonscm.debian.org/git/reproducible/notes.git/commit/?id=f096af1e)

Two issue types have been added:

- [new portable\_executable\_strong\_name\_varies](https://anonscm.debian.org/git/reproducible/notes.git/commit/?id=81607151)
- [captures\_build\_path\_in\_python\_sugar3\_symlinks](https://anonscm.debian.org/git/reproducible/notes.git/commit/?id=b2ab5e3b)

Weekly QA work
--------------

During our reproducibility testing, FTBFS bugs have been detected and reported by:

 - Chris Lamb (2)


diffoscope development
----------------------


- Ximin Luo:
  - [Add support for R rds and rdb object files](https://anonscm.debian.org/git/reproducible/diffoscope.git/commit/?id=4d31312)
  - [rdata: don't break if the .rdx isn't present](https://anonscm.debian.org/git/reproducible/diffoscope.git/commit/?id=d3a9429)

- Chris Lamb:
  - [Ensure r-base-core is available when testing.](https://anonscm.debian.org/git/reproducible/diffoscope.git/commit/?id=3d15b7e)
  - Misc cleanups

Misc.
-----

This week's edition was written by Chris Lamb, Vagrant Cascadian & reviewed by a bunch of Reproducible Builds folks on IRC & the mailing lists.
