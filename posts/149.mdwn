[[!meta title="Reproducible Builds: Weekly report #149"]]
[[!meta date="Wed Mar 07 03:21:26 2018 +0000"]]
[[!tag reproducible_builds Debian buildinfo diffoscope documentation dpkg ordering Outreachy python]]

Here's what happened in the [Reproducible Builds](https://reproducible-builds.org) effort between Sunday February 25 and Saturday March 3 2018:

 * ActiveState published an article called [Reproducible builds: Introducing predictability into your pipeline](https://www.activestate.com/blog/2018/03/reproducible-builds-introducing-predictability-your-pipeline).

 * [Will Thompson](https://willthompson.co.uk/) pointed out that [Python dict order was defined in the language spec](https://mail.python.org/pipermail/python-dev/2017-December/151283.html) in December 2017.

 * Helmut Grune filed Debian bug [[!bug 891435]], related to `.buildinfo` processing ("`dpkg-genbuildinfo`: Does not arch qualify `Installed-Build-Depends`").

 * There was [a sprited discussion on the debian-devel mailing list regarding Debian's reproducibility environment](https://lists.debian.org/debian-devel/2018/03/threads.html#00036)

 * A [Hamburg-based Mini-DebConf has been announced](https://lists.debian.org/debian-devel-announce/2018/02/msg00003.html) which is likely to bring together many people working on Reproducible Builds.

diffoscope development
----------------------

Version [91](https://tracker.debian.org/news/937141) was uploaded to unstable by Mattia Rizzolo.
It included [contributions](https://anonscm.debian.org/git/reproducible/diffoscope.git/log/?h=91)
already covered by posts of the previous weeks as well as new ones from:

- Chris Lamb:
    - Bug fixes:
        - [Additionally dissect the `classes.dex` file from `.apk` files.](https://anonscm.debian.org/git/reproducible/diffoscope.git/commit/?id=b692cbd) (Closes: [[!bug 890904]])
        - [Recursively reset the permissions of temporary directories prior to deletion.](https://anonscm.debian.org/git/reproducible/diffoscope.git/commit/?id=7345efa) (Closes: [[!bug 891363]])
    - Reporting:
        - [Print a nicer error message if you only specify one file by mistake.](https://anonscm.debian.org/git/reproducible/diffoscope.git/commit/?id=5541c76)
        - [Don't show progress bar if we passed --debug; it just gets in the way.](https://anonscm.debian.org/git/reproducible/diffoscope.git/commit/?id=c38d7ec)
    - Tidying:
        - [Avoid necessary indentation around unconditional control flow.](https://anonscm.debian.org/git/reproducible/diffoscope.git/commit/?id=c1d2d18)
        - [Drop unused imports.](https://anonscm.debian.org/git/reproducible/diffoscope.git/commit/?id=010fd51)
        - [Ensure 4-line indentation.](https://anonscm.debian.org/git/reproducible/diffoscope.git/commit/?id=e580d20)
        - [Tidy an unnecessary assignment.](https://anonscm.debian.org/git/reproducible/diffoscope.git/commit/?id=60b5768)
        - [Add whitespace around operators.](https://anonscm.debian.org/git/reproducible/diffoscope.git/commit/?id=e813b51)
        - [Add a blank line before (and after) nested definition.](https://anonscm.debian.org/git/reproducible/diffoscope.git/commit/?id=a5f4486)
        - [Don't use ambiguous `l` variable names.](https://anonscm.debian.org/git/reproducible/diffoscope.git/commit/?id=bac56a5)
        - [Drop a blank line.](https://anonscm.debian.org/git/reproducible/diffoscope.git/commit/?id=39bd9ea)
        - [Use more Pythonic `old_level` variable name (over `oldLabel`).](https://anonscm.debian.org/git/reproducible/diffoscope.git/commit/?id=e37c629)
        - [Add two spaces before inline comment.](https://anonscm.debian.org/git/reproducible/diffoscope.git/commit/?id=33a6e70)
        - [Import `LooseVersion` as `LooseVersion`.](https://anonscm.debian.org/git/reproducible/diffoscope.git/commit/?id=b35c2cc)
        - [Move DOS/MBR check into the testsuite.](https://anonscm.debian.org/git/reproducible/diffoscope.git/commit/?id=e41abe0)

- Mattia Rizzolo:
    - [Fix a `UnicodeDecodeError` with a non-UTF8 locale.](https://anonscm.debian.org/git/reproducible/diffoscope.git/commit/?id=7b8998e) and [fix for Python less than 3.6](https://anonscm.debian.org/git/reproducible/diffoscope.git/commit/?id=686f3af).
    - [Reset permissions only when we actually need to.](https://anonscm.debian.org/git/reproducible/diffoscope.git/commit/?id=99c0a78)

- Juliana Oliveira:
    - [Replace `.stderr_content` with `.stderr`.](https://anonscm.debian.org/git/reproducible/diffoscope.git/commit/?id=997a5f6)
    - Replace/remove calls to `.wait`: [(1)](https://anonscm.debian.org/git/reproducible/diffoscope.git/commit/?id=5d57eca) [(2)](https://anonscm.debian.org/git/reproducible/diffoscope.git/commit/?id=26db582)
    - [Replace `subprocess.Popen()` calls with `subprocess.run()`.](https://anonscm.debian.org/git/reproducible/diffoscope.git/commit/?id=f93fb4c)

In addition, Juliana — our Outreachy intern — continued her work on parallel processing; the above work is part of it.

reproducible-website development
--------------------------------

- Chris Lamb:
    - [Add support for pages that do not appear in the header bar](https://anonscm.debian.org/git/reproducible/reproducible-website.git/commit/?id=b00b1e3)

Packages reviewed and fixed, and bugs filed
-------------------------------------------

* Adrian Bunk:
    * [[!bug 891521]] filed against [[!pkg mccs]].
    * [[!bug 891523]] filed against [[!pkg tcpxtract]].
    * [[!bug 891529]] filed against [[!pkg acedb]].
    * [[!bug 891532]] filed against [[!pkg captagent]].
    * [[!bug 891533]] filed against [[!pkg chemeq]].
* Chris Lamb:
    * [[!bug 891405]] filed against [[!pkg gr-gsm]].
    * [[!bug 891412]] filed against [[!pkg opari2]].
    * [[!bug 891897]] filed against [[!pkg gexiv2]] ([merged upstream](https://bugzilla.gnome.org/show_bug.cgi?id=793984)).
    * [[!bug 891899]] filed against [[!pkg node-rollup]] ([forwarded upstream](https://github.com/rollup/rollup/pull/2024))
* Jeremy Bicha:
    * [[!bug 891640]] filed against [[!pkg arctica-greeter]].
* Bernhard M. Wiedemann:
    * [gstreamer-plugins-bad](https://build.opensuse.org/request/show/582064) (sort readdir, [upstreaming](https://bugzilla.gnome.org/show_bug.cgi?id=794069))
    * [clusterssh](https://build.opensuse.org/request/show/582197) (race, [upstreaming](https://sourceforge.net/p/clusterssh/support-requests/55/))
    * [gpgme](https://dev.gnupg.org/T3815) FTBFS-2021

An [issue with the pydoctor documentation generator](https://github.com/twisted/pydoctor/pull/146#issuecomment-351221274) was merged upstream.

Reviews of unreproducible packages
----------------------------------

73 package reviews have been added, 37 have been updated and 26 have been removed in this week,
adding to our knowledge about [identified issues](https://tests.reproducible-builds.org/debian/index_issues.html).

* [`nondeterminstic_ordering_in_gsettings_glib_enums_xml`](https://anonscm.debian.org/git/reproducible/notes.git/commit/?id=3a37e745)

Weekly QA work
--------------

During our reproducibility testing, FTBFS bugs have been detected and reported by:

 - Adrian Bunk (46)
 - Jeremy Bicha (4)

Misc.
-----

This week's edition was written by Chris Lamb, Mattia Rizzolo & reviewed by a bunch of Reproducible Builds folks on IRC & the mailing lists.
